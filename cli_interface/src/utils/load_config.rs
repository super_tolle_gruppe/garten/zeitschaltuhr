use std::fs::File;
use std::io::prelude::*;
use yaml_rust::yaml::{Yaml};
use yaml_rust::YamlLoader;


pub fn load_config () -> Yaml  {
    let file = "client_zeitschaltuhr.yaml";
    let mut file = File::open(file).expect("Unable to open file");
    let mut contents = String::new();
    file.read_to_string(&mut contents).expect("Unable to read file");
    let mut data = YamlLoader::load_from_str(&contents).unwrap();
    return data.remove(0);
}
