use serde_json::json;
use serde::{Deserialize, Serialize};


#[derive(Serialize, Deserialize)]
pub struct Key {
    kit: String,
    kty: String,
    alg: String,
    use: String,
    n: String,
    e: String,
    x5c: String,
    x5t: String,
} 

#[derive(Serialize, Deserialize)]
pub struct Keys {
    keys: Vec<Key>,
} 

pub fn parse_string_to_jwt(keys_as_string: String) -> Keys {
    let keys: Keys = serde_json::from_str(jwt_as_string.as_str()).unwrap();

    return keys;
}
