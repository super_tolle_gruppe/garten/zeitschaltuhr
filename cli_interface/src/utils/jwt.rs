use serde_json::json;
use serde::{Deserialize, Serialize};


#[derive(Serialize, Deserialize)]
pub struct Jwt {
    access_token: String,
    token_type: String,
    session_state: String,
    #[serde(rename = "not-before-policy")] 
    not_before_policy: i64,
    scope: String,
} 

pub fn parse_string_to_jwt(jwt_as_string: String) -> Jwt {
    let jwt: Jwt = serde_json::from_str(jwt_as_string.as_str()).unwrap();

    return jwt;
}
