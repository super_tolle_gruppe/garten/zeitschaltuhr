use argh::FromArgs;
use std::path::{PathBuf};

/// Parameter für den den Client
#[derive(FromArgs)]
pub struct Options {
    /// host
    #[argh(positional)]
    pub host: String,

    /// port
    #[argh(option, short = 'p', default = "443")]
    pub  port: u16,

    /// domain
    #[argh(option, short = 'd')]
    pub domain: Option<String>,

    /// cafile
    #[argh(option, short = 'c')]
    pub cafile: Option<PathBuf>,
}
