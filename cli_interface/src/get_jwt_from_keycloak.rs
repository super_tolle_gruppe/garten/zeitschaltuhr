#[path = "utils/start_parameter_options.rs"] mod start_parameter_options;
#[path = "utils/jwt.rs"] mod jwt;
use actix_web::client::Client;
use std::error::Error;
use std::str::FromStr;
use std::time::Duration;
use actix_http::client::Connector;
use std::sync::Arc;
use std::fs::File;
use std::{io, str};
use std::io::BufReader;
//use std::net::ToSocketAddrs;
//use tokio::io::{copy, split, stdin as tokio_stdin, stdout as tokio_stdout, AsyncWriteExt};
//use tokio::net::TcpStream;
//use tokio_rustls::{rustls::ClientConfig, webpki::DNSNameRef, TlsConnector};
use actix_connect::ssl::rustls::ClientConfig;
use actix_http::body::Body;
use serde_json::json;
use serde::{Deserialize, Serialize};


use yaml_rust::yaml::{Yaml};


pub async fn get_jwt_from_keycloak(zeitschaltuhr_config: &Yaml, login_user:&String, passwort:&String) ->Result<String, String> {
    let mut client_config = ClientConfig::new();
    if let Some(cafile) = zeitschaltuhr_config["ca_file"].as_str() {
        let open_file = File::open(cafile).unwrap();
        let mut pem = BufReader::new(open_file);
        let t = client_config
            .root_store
            .add_pem_file(&mut pem)
            .map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "invalid cert"));

        if t.is_err() {
            return Err(t.unwrap_err().to_string());
        }
    } 


    let arc_client_config = Arc::new(client_config);
    let connector = Connector::new().timeout(Duration::from_secs(30)).rustls(arc_client_config).finish();
    let client = Client::builder()
        .connector(connector)
        .finish();
    
    let client_id = zeitschaltuhr_config["client_id"].as_str().unwrap();
    let client_secret = zeitschaltuhr_config["client_secret"].as_str().unwrap();

    let post_body = format!("grant_type=password&client_id={}&client_secret={}&username={}&password={}", client_id, client_secret,login_user,passwort);
    let keycloak_address = zeitschaltuhr_config["domain"].as_str().unwrap().to_string();
    let keycloak_port_t = zeitschaltuhr_config["port"].as_i64();
    let keycloak_port = keycloak_port_t.unwrap().to_string();
    let url = format!(r"https://{}:{}/auth/realms/zeitschaltuhr/protocol/openid-connect/token", keycloak_address,keycloak_port);
    let timeout = Duration::from_secs(180);
    let response2 = client.post(url)
        .header("Accept-Encoding", "gzip,deflate")
        .header(http::header::CONTENT_TYPE, "application/x-www-form-urlencoded")
        .timeout(timeout)
        .send_body(post_body)
        .await;

    let re:Result<String, String>;

    if response2.is_ok() {
        let mut r = response2.unwrap();
        let mb = r.body().limit(5242880).await.unwrap();
        let jwt_as_string = String::from_str(std::str::from_utf8(&mb).unwrap()).unwrap();
        if jwt_as_string.contains("\"error\":") {
            re = Err(format!("Response from Keycloak contains an error --> {}", jwt_as_string));
        } else {
            let jwt = jwt::parse_string_to_jwt(jwt_as_string);
            re = Ok(String::from_str(std::str::from_utf8(&mb).unwrap()).unwrap());
        }
    } else {
        println!("No Token from Keycloak.\n{:?}",response2);   //handled error
        re = Err(response2.unwrap_err().to_string());
    }
    return re;
}

pub async fn get_cert_from_keycloak(zeitschaltuhr_config: &Yaml, login_user:&String, passwort:&String) ->Result<String, String> {
    let mut client_config = ClientConfig::new();
    if let Some(cafile) = zeitschaltuhr_config["ca_file"].as_str() {
        let open_file = File::open(cafile).unwrap();
        let mut pem = BufReader::new(open_file);
        let t = client_config
            .root_store
            .add_pem_file(&mut pem)
            .map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "invalid cert"));

        if t.is_err() {
            return Err(t.unwrap_err().to_string());
        }
    } 


    let arc_client_config = Arc::new(client_config);
    let connector = Connector::new().timeout(Duration::from_secs(30)).rustls(arc_client_config).finish();
    let client = Client::builder()
        .connector(connector)
        .finish();
    
    // let client_id = zeitschaltuhr_config["client_id"].as_str().unwrap();
    // let client_secret = zeitschaltuhr_config["client_secret"].as_str().unwrap();

    // let post_body = format!("grant_type=password&client_id={}&client_secret={}&username={}&password={}", client_id, client_secret,login_user,passwort);
    let keycloak_address = zeitschaltuhr_config["domain"].as_str().unwrap().to_string();
    let keycloak_port_t = zeitschaltuhr_config["port"].as_i64();
    let keycloak_port = keycloak_port_t.unwrap().to_string();
    let url = format!(r"https://{}:{}/auth/realms/zeitschaltuhr/protocol/openid-connect/certs", keycloak_address,keycloak_port);
    let timeout = Duration::from_secs(180);
    let response2 = client.get(url)
        .header("Accept-Encoding", "gzip,deflate")
        .header(http::header::CONTENT_TYPE, "application/x-www-form-urlencoded")
        .timeout(timeout)
        .send()
        .await;

    let re:Result<String, String>;

    if response2.is_ok() {
        let mut r = response2.unwrap();
        let mb = r.body().limit(5242880).await.unwrap();
        let cert_as_string = String::from_str(std::str::from_utf8(&mb).unwrap()).unwrap();
        if cert_as_string.contains("\"error\":") {
            re = Err(format!("Response from Keycloak contains an error --> {}", cert_as_string));
        } else {
            println!("Cert:\n{}",cert_as_string);   
            // let jwt = jwt::parse_string_to_jwt(cert_as_string);
            re = Ok(String::from_str(std::str::from_utf8(&mb).unwrap()).unwrap());
        }
    } else {
        println!("No Token from Keycloak.\n{:?}",response2);   //handled error
        re = Err(response2.unwrap_err().to_string());
    }
    return re;
}