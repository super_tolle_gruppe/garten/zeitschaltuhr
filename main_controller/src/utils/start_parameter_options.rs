use argh::FromArgs;
use std::path::{PathBuf};

/// Tokio Rustls server example
#[derive(FromArgs)]
pub struct Options {
    /// bind addr
    #[argh(positional)]
    pub addr: String,

    /// cert file
    #[argh(option, short = 'c')]
    pub cert: PathBuf,

    /// key file
    #[argh(option, short = 'k')]
    pub key: PathBuf,

    /// echo mode
    #[argh(switch, short = 'e')]
    pub echo_mode: bool,
}